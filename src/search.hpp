#include <iostream>
#include <string>

using namespace std;

int search(auto text, auto pattern)
{
	for (int i=0; i < text.size() - pattern.size(); i++)
	{
		int j;
		
		for (j=0; j < pattern.size(); j++)
			if (text[i+j] != pattern[j])
				break;
				
			if (text[j]==pattern)		
		return j;
	}
	return -1;
}
